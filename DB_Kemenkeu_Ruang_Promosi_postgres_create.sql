CREATE TABLE "User" (
	"id" serial NOT NULL,
	"email" VARCHAR(225) NOT NULL,
	"password" VARCHAR(225) NOT NULL,
	"fullname" VARCHAR(225) NOT NULL,
	"address" VARCHAR(225),
	"npwp_upload_data_id" integer NOT NULL,
	"ktp_upload_data_id" integer NOT NULL,
	"isLogin?" BOOLEAN NOT NULL DEFAULT 'false',
	"token" VARCHAR(255) NOT NULL,
	CONSTRAINT "User_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);


CREATE TABLE "Upload_Data" (
	"id" serial NOT NULL,
	"path" VARCHAR(255) NOT NULL,
	"description" VARCHAR(255) NOT NULL,
	CONSTRAINT "Upload_Data_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "Umkm" (
	"user_id" integer NOT NULL,
	"id" serial NOT NULL,
	"business_name" VARCHAR(225) NOT NULL,
	"categori_id" integer NOT NULL,
	"pic" VARCHAR(225) NOT NULL,
	"pic_addres" VARCHAR(225) NOT NULL,
	"ktp_upload_data_id" integer NOT NULL,
	"npwp_upload_data_id" integer NOT NULL,
	CONSTRAINT "Umkm_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "Promotion" (
	"payment_id" integer NOT NULL,
	"id" serial NOT NULL UNIQUE,
	"title" VARCHAR(255) NOT NULL UNIQUE,
	"start_date" DATE NOT NULL,
	"finish_date" DATE NOT NULL,
	"current_target" integer NOT NULL,
	"promotion_target" integer NOT NULL,
	"text_materail" VARCHAR(255) NOT NULL,
	"promotion_status" VARCHAR(255) NOT NULL,
	"photos_upload_data_id" integer NOT NULL,
	CONSTRAINT "Promotion_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "Payment" (
	"user_id" serial NOT NULL,
	"id" serial NOT NULL,
	"total_invoice" serial NOT NULL,
	"payment_status" VARCHAR(255) NOT NULL,
	"isPaid?" BOOLEAN NOT NULL DEFAULT 'false',
	CONSTRAINT "Payment_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "Category" (
	"id" serial NOT NULL,
	"type" VARCHAR(255) NOT NULL,
	CONSTRAINT "Category_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



ALTER TABLE "User" ADD CONSTRAINT "User_fk0" FOREIGN KEY ("npwp_upload_data_id") REFERENCES "Upload_Data"("id");
ALTER TABLE "User" ADD CONSTRAINT "User_fk1" FOREIGN KEY ("ktp_upload_data_id") REFERENCES "Upload_Data"("id");


ALTER TABLE "Umkm" ADD CONSTRAINT "Umkm_fk0" FOREIGN KEY ("user_id") REFERENCES "User"("id");
ALTER TABLE "Umkm" ADD CONSTRAINT "Umkm_fk1" FOREIGN KEY ("categori_id") REFERENCES "Category"("id");
ALTER TABLE "Umkm" ADD CONSTRAINT "Umkm_fk2" FOREIGN KEY ("ktp_upload_data_id") REFERENCES "Upload_Data"("id");
ALTER TABLE "Umkm" ADD CONSTRAINT "Umkm_fk3" FOREIGN KEY ("npwp_upload_data_id") REFERENCES "Upload_Data"("id");

ALTER TABLE "Promotion" ADD CONSTRAINT "Promotion_fk0" FOREIGN KEY ("payment_id") REFERENCES "Payment"("id");
ALTER TABLE "Promotion" ADD CONSTRAINT "Promotion_fk1" FOREIGN KEY ("id") REFERENCES "Umkm"("id");
ALTER TABLE "Promotion" ADD CONSTRAINT "Promotion_fk2" FOREIGN KEY ("photos_upload_data_id") REFERENCES "Upload_Data"("id");

ALTER TABLE "Payment" ADD CONSTRAINT "Payment_fk0" FOREIGN KEY ("user_id") REFERENCES "User"("id");








